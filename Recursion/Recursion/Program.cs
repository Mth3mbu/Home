﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Recursion
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string letters = "ACMICPC";

            List<Stack> stackList = new List<Stack>();

            Console.WriteLine($"For loop version:{GetStacks(letters)}\nRecursion version:{GetStacksRecursion(letters,0,stackList)}");

            Console.ReadKey();

        }

        public static int GetStacks(string letters)
        {
            List<Stack> stackList = new List<Stack>();
            
            for (int i = 0; i < letters.Length; i++)
            {
                if (stackList.Any(x => (char)x.Peek() >= letters[i]))
                {
                    int index = stackList.Where(x => (char)x.Peek() >= letters[i]).Select(x => stackList.IndexOf(x)).First<int>();
                    stackList[index].Push(letters[i]);
                }
                else
                {
                    stackList.Add(new Stack());
                    stackList[stackList.Count - 1].Push(letters[i]);
                }
            }

            return stackList.Count;
        }

        public static int GetStacksRecursion(string letters, int i, List<Stack> stackList)
        {
            if (i >= letters.Length)
            {
                return stackList.Count;
            }

            if (stackList.Any(x => (char)x.Peek() >= letters[i]))
            {
                int index = stackList.Where(x => (char)x.Peek() >= letters[i]).Select(x => stackList.IndexOf(x)).First<int>();
                stackList[index].Push(letters[i]);
            }
            else
            {
                stackList.Add(new Stack());
                stackList[stackList.Count - 1].Push(letters[i]);
            }

            GetStacksRecursion(letters, i+1, stackList);
           
            return stackList.Count;
        }
    }
}